#include "CommandGenerator.h"

CommandGenerator::CommandGenerator(QObject *parent) : QObject(parent)
{

}

int32_t CommandGenerator::CreateCommand(char* outer_buf, TreeNode* node, QString write_text)
{
    int32_t total_len = 10;
    int32_t data_len = 2;
    int8_t section_code = 0;
    int8_t cmd_code = 0;
    char* write_data = nullptr;

    QMap <QString, QVariant> cmd_map = node->item_data_.toMap();
    QMap <QString, QVariant> section_map = node->parent_item_->item_data_.toMap();

    bool ok;
    section_code = static_cast<char>(section_map["sectionCode"].toString().toInt(&ok, 16));
    cmd_code = static_cast<char>(cmd_map["Code"].toString().toInt(&ok, 16));

    if(write_text != nullptr)
    {
        QByteArray arr = write_text.toLocal8Bit();
        write_data = arr.data();
        int32_t write_data_len = write_text.length();
        data_len +=write_data_len;
        total_len += write_data_len;

        memcpy(outer_buf + 6, write_data, static_cast<size_t>(data_len));
    }

    outer_buf[0] = 0x7e;
    outer_buf[1] = static_cast<char>( data_len );
    outer_buf[2] = static_cast<char>( data_len << 8 );
    outer_buf[3] = 0x55;
    outer_buf[4] = section_code;
    outer_buf[5] = cmd_code;

    outer_buf[data_len + 4] = static_cast<char>(0xff);
    outer_buf[data_len + 5] = static_cast<char>(0xff);
    outer_buf[data_len + 6] = static_cast<char>(0xbd);
    outer_buf[data_len + 7] = static_cast<char>(0x00);

    return total_len;
}
