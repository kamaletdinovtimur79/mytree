#pragma once

#include <QVariant>
#include <QVector>
#include <QDebug>
struct TreeNode
{
    QVector<TreeNode*> child_items_;
    QVariant item_data_;
    TreeNode* parent_item_;
};

class Tree
{
public:
    Tree()
    {
        root_ = new TreeNode;
        root_->item_data_ = "";
        root_->parent_item_ = nullptr;
    }

    TreeNode* Insert(TreeNode * parent, QVariant item_data)
    {
        if(parent == nullptr) return Insert(root_, item_data);

        TreeNode* child = new TreeNode;

        child->item_data_ = item_data;

        child->parent_item_ = parent;

        parent->child_items_.append(child);

        return child;
    }

    TreeNode * Search(QString key, QString data)
    {
        return Search(root_, key, data);
    }

    void PrintTree()
    {
        PrintTree(root_);
    }

    TreeNode * GetRoot()
    {
        return root_;
    }
private:
    void PrintTree(TreeNode * parent)
    {
        qDebug() << parent->item_data_;

        for (int i = 0; i < parent->child_items_.size(); ++i)
        {
            PrintTree(parent->child_items_.at(i));
        }
    }

    TreeNode * Search(TreeNode * parent, QString key, QString data)
    {
        QVariantMap map = parent->item_data_.toMap();

        if ( map[key] == data )
        {
            return parent;
        }
        for (int i = 0; i < parent->child_items_.size(); i++)
        {
            TreeNode* node = Search(parent->child_items_.at(i), key, data);

            if(node != nullptr)
            {
                return node;
            }
        }
        return nullptr;
    }

    TreeNode * root_;
};
