#pragma once
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QFile>
#include "Tree.h"

class JsonParser
{
public:
    JsonParser();
    Tree* GetTree(QJsonObject obj);
private:
    void GetTree(QJsonObject parent_obj, TreeNode* parent);
    void GetTree(QJsonArray parent_obj, TreeNode* parent);


    QJsonObject json_;
    Tree* tree_;
};
