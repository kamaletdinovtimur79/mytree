#include <QDebug>
#include <QVariantMap>
#include <QKeyEvent>
#include <QInputDialog>

#include "TreeWidget.h"


TreeWidget::TreeWidget(Tree* tree, QWidget *parent) :
    QTreeWidget(parent),
    tree_(tree)
{


    setWindowTitle("layouts");
    setFixedWidth(600);
    setFixedHeight(600);
    QTreeWidgetItem* header = this->headerItem();

    header->setText(0,"Commands");

    CreateTreeWidget(tree_);

    com_gen = new CommandGenerator();
    connect(this, &QTreeWidget::itemClicked ,  this,  &TreeWidget::slotItemChanged);
}

void TreeWidget::slotItemChanged(QTreeWidgetItem* item, int column)
{

    char cmd_buf[0x100];

    QString cmd_name = item->text(column);

    QString write_text = nullptr;

    TreeNode * node = tree_->Search("ObjectName", cmd_name);

    if (node->child_items_.size() != 0)
    {
        return;
    }
    if(key_pressed_ == true)
    {
        bool ok;

        write_text = QInputDialog::getText(this, "Input data", "Data:", QLineEdit::Normal, "", &ok);

        key_pressed_ = false;
    }

    if(node != nullptr)
    {
        QMap <QString, QVariant> map = node->item_data_.toMap();

        qDebug() << map["ObjectName"].toString();

        int32_t cmd_len = com_gen->CreateCommand(cmd_buf, node, write_text);

        qDebug() << QByteArray::fromRawData(cmd_buf, cmd_len).toHex();
    }
    else
    {
        qDebug() << "fail";
    }

}

void TreeWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Control)
    {
        // handle the key press, perhaps giving the item text a default value
        event->accept();
        key_pressed_ = true;
    }
    else
    {
        QTreeView::keyPressEvent(event); // call the default implementation
    }
}

void TreeWidget::keyReleaseEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Control)
    {
        // handle the key press, perhaps giving the item text a default value
        event->accept();
        key_pressed_ = false;
    }
    else
    {
        QTreeView::keyPressEvent(event); // call the default implementation
    }
}

void TreeWidget::CreateTreeWidget(Tree * tree)
{
    CreateTreeWidget(nullptr, tree->GetRoot());
}

void TreeWidget::CreateTreeWidget(QTreeWidgetItem * parent, TreeNode * node)
{
    QTreeWidgetItem * item = nullptr;
    QMap <QString, QVariant> map = node->item_data_.toMap();
    if(node->parent_item_ != nullptr && map.find("ObjectName") != map.end())
    {    
        if(parent == nullptr)
        {
            item = new QTreeWidgetItem(0);
            addTopLevelItem(item);
        }
        else
        {
            item = new QTreeWidgetItem(parent);
        }
        item->setText(0, map["ObjectName"].toString());
    }
    for (int i = 0; i < node->child_items_.size(); ++i)
    {
        CreateTreeWidget(item, node->child_items_.at(i));
    }
}


