#pragma once

#include <QObject>
#include <QVariant>
#include "Tree.h"
class CommandGenerator : public QObject
{
    Q_OBJECT
public:
    explicit CommandGenerator(QObject* parent = nullptr);

signals:

public slots:
    int32_t CreateCommand(char* outer_buf, TreeNode* node, QString write_data);

};
