#pragma once
#include <QTreeWidget>

#include "Tree.h"
#include "CommandGenerator.h"

class TreeWidget : public QTreeWidget
{
     Q_OBJECT
public:
    TreeWidget(Tree* tree, QWidget *parent = nullptr);
public slots:
    void slotItemChanged(QTreeWidgetItem *item, int column);
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
private:
    void CreateTreeWidget(Tree* tree);
    void CreateTreeWidget(QTreeWidgetItem* parent, TreeNode* node);
    
    Tree* tree_;
    volatile bool key_pressed_ = false;
    CommandGenerator * com_gen;
};

