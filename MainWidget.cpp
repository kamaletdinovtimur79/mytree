#include "MainWidget.h"
#include "TreeWidget.h"
#include "JsonParser.h"
#include <QLayout>
#include <QDebug>

MainWidget::MainWidget(QWidget *parent) : QWidget(parent)
{

    QString currentPath_ = "C:/Users/tkn/Documents/mytree";

    QFile loadFile(currentPath_ + "/commands.json");

    if (loadFile.open(QIODevice::ReadOnly))
    {
        qDebug() << "QFile Ok";
    }

    QByteArray JsonData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(JsonData));

    QJsonObject json = loadDoc.object();

    JsonParser jsp;

    Tree* tree = jsp.GetTree(json);

    TreeWidget* tree_widget = new TreeWidget(tree);

    QVBoxLayout* vLayout = new QVBoxLayout;

    vLayout->addWidget(tree_widget);

    QVBoxLayout* mainLayout = new QVBoxLayout;

    mainLayout->addLayout(vLayout);

    setLayout(mainLayout);

}
