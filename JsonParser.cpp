#include "JsonParser.h"
#include <QDebug>


JsonParser::JsonParser()
{

}

Tree* JsonParser::GetTree(QJsonObject obj)
{
    Tree* tree = new Tree;
    GetTree(obj, tree->GetRoot());
    return tree;
}

void JsonParser::GetTree(QJsonObject obj, TreeNode* parent_node)
{
    foreach(const QString& key, obj.keys())
    {
        if(obj[key].isArray() == true)
        {
            QJsonArray arr = obj[key].toArray();
            for (int i = 0; i < arr.size(); ++i)
            {
                QJsonObject new_obj = arr[i].toObject();
                TreeNode* child = tree_->Insert(parent_node, new_obj.toVariantMap());
                GetTree(new_obj, child);
            }
        }
        if(obj[key].isObject())
        {
            QJsonObject new_obj = obj[key].toObject();
            TreeNode* child = tree_->Insert(parent_node, new_obj.toVariantMap());
            GetTree(new_obj, child);
        }
    }

}
